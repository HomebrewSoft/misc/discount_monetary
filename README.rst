Discount Monetary
=================

Adds the discount in the sale.order, sale.order.line, account.invoice and account.invoice.line to see the amount discounted.
