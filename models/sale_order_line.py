# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'

    discount_monetary = fields.Monetary(
        compute='_get_discount_monetary',
    )

    @api.depends('product_uom_qty', 'price_unit', 'discount')
    def _get_discount_monetary(self):
        for record in self:
            record.discount_monetary = record.product_uom_qty * record.price_unit * record.discount / 100
