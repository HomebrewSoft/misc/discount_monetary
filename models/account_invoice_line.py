# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class AccountInvoiceLine(models.Model):
    _inherit = 'account.invoice.line'

    discount_monetary = fields.Monetary(
        compute='_get_discount_monetary',
    )

    @api.depends('quantity', 'price_unit', 'discount')
    def _get_discount_monetary(self):
        for record in self:
            record.discount_monetary = record.quantity * record.price_unit * record.discount / 100
