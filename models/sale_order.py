# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    discount_monetary = fields.Monetary(
        compute='_get_discount_monetary',
    )

    @api.depends('order_line')
    def _get_discount_monetary(self):
        for record in self:
            record.discount_monetary = sum([line.discount_monetary for line in record.order_line])
