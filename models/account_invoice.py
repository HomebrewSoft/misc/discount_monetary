# -*- coding: utf-8 -*-
from odoo import _, api, fields, models


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    discount_monetary = fields.Monetary(
        compute='_get_discount_monetary',
    )

    @api.depends('invoice_line_ids')
    def _get_discount_monetary(self):
        for record in self:
            record.discount_monetary = sum([line.discount_monetary for line in record.invoice_line_ids])
