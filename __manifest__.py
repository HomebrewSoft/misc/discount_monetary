# -*- coding: utf-8 -*-
{
    'name': 'Discount Monetary',
    'version': '12.0.0.1.1',
    'author': 'HomebrewSoft',
    'website': 'https://homebrewsoft.dev/',
    'license': 'LGPL-3',
    'depends': [
        'account',
        'sale',
    ],
    'data': [
        # security
        # data
        # reports
        # views
    ],
}
